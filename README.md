# swagger2

#### 介绍
Swagger™的目标是为REST API 定义一个标准的，与语言无关的接口，使人和计算机在看不到源码或者看不到文档或者不能通过网络流量检测的情况下能发现和理解各种服务的功能。当服务通过Swagger定义，消费者就能与远程的服务互动通过少量的实现逻辑。简单说，swagger能够根据代码中的注释自动生成api文档，并且提供测试接口

#### 软件架构
SpringBoot2整合swagger2的一些用法


#### 使用说明

>@Api：用在请求的类上，表示对类的说明  
>>tags="说明该类的作用，可以在UI界面上看到的注解"  
>>value="该参数没什么意义，在UI界面上也看到，所以不需要配置"  
  
>@ApiOperation：用在请求的方法上，说明方法的用途、作用  
>>value="说明方法的用途、作用"  
>>notes="方法的备注说明"  

>@ApiImplicitParams：用在请求的方法上，表示一组参数说明  
>>@ApiImplicitParam：用在@ApiImplicitParams注解中，指定一个请求参数的各个方面  
>>>name：参数名  
>>>value：参数的汉字说明、解释  
>>>required：参数是否必须传  
>>>paramType：参数放在哪个地方  
>>>>· header --> 请求参数的获取：@RequestHeader  
>>>>· query --> 请求参数的获取：@RequestParam  
>>>>· path（用于restful接口）--> 请求参数的获取：@PathVariable  
>>>>· body（不常用）  
>>>>· form（不常用）  

>>>dataType：参数类型，默认String，其它值dataType="int"          
>>>defaultValue：参数的默认值  

>@ApiResponses：用在请求的方法上，表示一组响应  
>>@ApiResponse：用在@ApiResponses中，一般用于表达一个错误的响应信息  
>>>code：数字，例如400  
>>>message：信息，例如"请求参数没填好"  
>>>response：抛出异常的类 
 
>@ApiModel：用于响应类上，表示一个返回响应数据的信息  
           （这种一般用在post创建的时候，使用@RequestBody这样的场景，  请求参数无法使用@ApiImplicitParam注解进行描述的时候）  
>>@ApiModelProperty：用在属性上，描述响应类的属性  

