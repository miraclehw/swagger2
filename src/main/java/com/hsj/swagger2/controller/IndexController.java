package com.hsj.swagger2.controller;

import com.hsj.swagger2.dto.JsonResult;
import com.hsj.swagger2.pojo.User;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author:Teacher黄
 * @date:Created at 2019/09/17
 */



@Api(tags = "主页接口")
@RestController
public class IndexController {

    @Autowired
    private User user;


    /**
     * 主页--无参数
     * @return
     */
    @ApiOperation(value = "用户主页")
    @GetMapping("/index")
    public User index(){
        user.setId(11);
        user.setUserName("tom");
        user.setUserAge(22);
        return user;
    }


    /**
     * 获取用户信息--路径参数
     * @param id : 用户id
     * @param userName : 用户名
     * @param age : 用户年龄
     *
     * @return
     */
    @ApiOperation(value = "获取用户信息",notes = "创建用户")
    @ApiImplicitParams({
            // paramType : 如果为path传参 必须为path  ,  如果为get/post传参 必须为query
            @ApiImplicitParam(name = "id",value = "用户id",required = true,defaultValue = "1",dataType = "int",paramType = "path"),
            @ApiImplicitParam(name = "name",value = "用户名字",required = true,defaultValue = "hsj",dataType = "String",paramType = "path"),
            @ApiImplicitParam(name = "age",value = "用户年龄",required = true,defaultValue = "11",dataType = "int",paramType = "path")
    })
    @GetMapping("/get/user/{id}/{name}/{age}")
    public User getUser(@PathVariable Integer id,
                        @PathVariable(name = "name") String userName,
                        @PathVariable Integer age){

        user.setId(id);
        user.setUserName(userName);
        user.setUserAge(age);
        return user;
    }


    /**
     * 获取用户数据2--json注入
     * @param user
     * @return
     */
    @ApiOperation(value = "获取用户信息",notes = "通过json注入数据")
    @ApiImplicitParam(name = "user",value = "用户对象json",required = true,dataType = "User")
    @ApiResponses({
            @ApiResponse(code = 200,message = "获取数据成功"),
            @ApiResponse(code = 400,message = "获取数据失败",response = Exception.class)
    })
    @PostMapping("/get/user")
    public User getUser2(@RequestBody User user){
        return user;
    }


    /**
     * 获取用户数据3--普通请求参数
     * @return
     */
    @ApiOperation(value = "获取用户信息3",notes = "通过正常请求参数获取用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "用户id",required = true,dataType = "int",paramType = "query"),
            @ApiImplicitParam(name = "userName",value = "用户名",required = true,dataType = "String",paramType = "query")
    })
    @PostMapping("/get/user3")
    public User getUser3(Integer id, String userName){
        user.setId(id);
        user.setUserName(userName);
        return user;
    }


    /**
     * 获取用户信息4
     * @return
     */
    @ApiOperation(value = "获取用户信息4",notes = "验证@ApiModel")
    @PostMapping("/get/user4")
    public JsonResult<User> getUser4(){
        user.setId(100);
        user.setUserName("hsj");
        user.setUserAge(100);
        return new JsonResult<>(200,"获取成功",user);
    }



}
