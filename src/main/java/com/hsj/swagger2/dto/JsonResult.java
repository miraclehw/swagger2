package com.hsj.swagger2.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * @author:Teacher黄
 * @date:Created at 2019/09/18
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@ApiModel(value = "Json返回类")
public class JsonResult<T> {
    @NonNull
    @ApiModelProperty(value = "状态码")
    private Integer code;
    @NonNull
    @ApiModelProperty(value = "提示信息")
    private String msg;
    @ApiModelProperty(value = "返回的对象")
    private T data;
}
