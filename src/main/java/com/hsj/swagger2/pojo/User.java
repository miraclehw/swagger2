package com.hsj.swagger2.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.stereotype.Component;

/**
 * @author:Teacher黄
 * @date:Created at 2019/09/17
 */
@Component
@Getter
@Setter
@ToString
@NoArgsConstructor
@ApiModel(value = "用户类")
public class User {

    @ApiModelProperty(value = "用户id")
    private Integer id;
    @ApiModelProperty(value = "用户名")
    private String userName;
    @ApiModelProperty(value = "用户年龄")
    private Integer userAge;

}
