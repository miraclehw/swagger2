package com.hsj.swagger2.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @author:Teacher黄
 * @date:Created at 2019/09/17
 */
@Configuration
public class Swagger2Config {

    @Value("${swaggerShow.show}")
    private Boolean swaggerShow;

    @Bean
    public Docket createRestApi(){
        if(swaggerShow){
            return new Docket(DocumentationType.SWAGGER_2)
                    .apiInfo(apiInfo())
                    .select()
                    .apis(RequestHandlerSelectors.basePackage("com.hsj.swagger2.controller"))
                    .paths(PathSelectors.any())
                    .build();
        }else {
            return new Docket( DocumentationType.SWAGGER_2)
                    .apiInfo(apiInfo())
                    .select()
                    .apis( RequestHandlerSelectors.basePackage("cmo.00.00.0.0"))
                    .paths( PathSelectors.any())
                    .build();
        }
    }

    /**
     * api相关信息
     * @return
     */
    private ApiInfo apiInfo(){
        if(swaggerShow){
            return new ApiInfoBuilder()
                    .title("springBoot利用Swagger创建api文档")
                    .description("测试swagger2")
                    .version("v1.0")
                    .build();
        }else{
            return new ApiInfoBuilder()
                    .title("项目生产阶段禁止使用swagger查看接口")
                    .description("基于springBoot的整合开发")
                    .termsOfServiceUrl("http://www.baodu.com/")
                    .contact("")
                    .version("")
                    .build();
        }
    }

}
